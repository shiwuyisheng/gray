package com.xlz.gray.mapper;

import com.xlz.commons.base.mapper.BaseMapper;
import com.xlz.gray.model.Setting;

/**
 *
 * Setting 表数据库控制层接口
 *
 */
public interface SettingMapper extends BaseMapper<Setting> {

}